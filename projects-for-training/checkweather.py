"""Check weather based upon address. 

Requires api key as api_key.txt as a local-to-script file.
This file should only have one line, and it should be your
own api key that you have granted Google geocode API 
permissions to in your own GCP console.

Usage:

  python3 check-weather.py
"""
from os.path import exists
import requests
import json
import googlemaps
from rich.panel import Panel
from rich import print as rprint
from rich.console import Console
from rich.text import Text
from rich.table import Table
console = Console()

class WeatherFunctions:

    def __init__(self):
        pass
    
    def checkforrequirements():
        """This function will fail our program if our needed file
    
    does not exist.

    Returns:
        boolean or exits script
    """
    if not exists("api_key.txt"):
        print()
        print("This script requires the use of an API key from Google\n",
        "as well as a couple pip modules to be installed.\n"
        "You must have api_key.txt in this directory.")
        print()
        exit()
    # return

    
    # prompt user for the address they wish to get a forecast for
    def getthelocation():
        """Prompt user for the address they wish to search.
        
        This function will open the api_key.txt file, prompt the
        user for the address they want to get the weather
        forecast for.  This function returns the Latitdue and
        Logitude for the weather lookup. It further prints the
        full address to the screen in Google geocode format

        Returns:
            A dictionary of Latitude and Longitude
        """
        # obtain the API key from our text file api_key.txt 
        apikeyfile = open('api_key.txt', 'r')
        apikeyvar = apikeyfile.readline()
        apikeyfile.close()
        gmaps = googlemaps.Client(key=apikeyvar)

        print()
        print("Please enter your street address. No Zip necessary:")
        addresstosearch = input("e.g. 123 N Center Street, Centerville, AL:\n")
        print()

        # variable geocode_result is the returned object from Google's API
        geocode_result = gmaps.geocode(addresstosearch)
        forpanel = geocode_result[0]['formatted_address']
        sometext = Text()
        sometext.append("Your address is: ",)
        sometext.append(forpanel, style="bold yellow")
        console.print(Panel.fit(sometext, border_style="red"))
        return geocode_result[0]['geometry']['location']


    def gettheweather(lat_var, lon_var):
        """This function accepts two float values to send to 
        
        https://api.weather.gov api site.  This will in turn
        receive a json object of the weather forecast for 
        that geographical area. json is parsed later.

        Args:
            lat_var: float of Latitude
            lon_var: float of Longitude

        Returns:
            json object to be parsed later
        """
        latitudevar = format(lat_var, '.4f')
        longitudevar = format(lon_var, '.4f')
        noaaurl = "https://api.weather.gov/points/"+latitudevar+','+longitudevar
        getforecasturl = requests.get(noaaurl)  # this gets the object into memory
        noaajson = getforecasturl.content  # here I am taking the object and turning it into a string
        obtainurl = json.loads(noaajson)  # here I am taking the string and turning it into json
        # this parses the json and  gets the URL for the actual forecast
        noaaurlforecast = obtainurl['properties']['forecast']
        getobject = requests.get(noaaurlforecast)  # this gets the object into memory
        forecastcontent = getobject.content        # here I am taking the object and turning it into a string
        forecastjson = json.loads(forecastcontent)  # here I am taking the string and turning it into json
        print(forecastjson)
        return forecastjson

    def askuserhowmanydays():  
        """Here we ask the user for how many days forcast
        
        Returns:
            doublingit: is a valude twice the requested forecast since 
            each day has both daytime and nighttime forecast. This input will 
            have to be checked that it is an integer value.
        """
        howmanydays = int(input("How many days forecast do you want? (1-7): "))
        doublingit = howmanydays * 2
        return doublingit


def main():
    """This is main and will call related functions
    
    Here we will also be printing the output to the screen
    """
    WeatherFunctions.checkforrequirements()
    days = WeatherFunctions.askuserhowmanydays()
    xandy = WeatherFunctions.getthelocation()
    lati_var = xandy['lat']
    long_var = xandy['lng']
    data = WeatherFunctions.gettheweather(lati_var, long_var)

    table = Table(title="Your Weather Forecast", title_justify="left", show_lines=True)
    table.add_column("Day", justify="left", style="cyan", no_wrap=True)
    table.add_column("Temperature", justify="center", style="magenta")
    table.add_column("Forecast", justify="left", style="green")

    for i in range(int(days)):
        templist = []
        templist.append(json.dumps(data['properties']['periods'][i]["name"]))
        templist.append(json.dumps(data['properties']['periods'][i]["temperature"]))
        templist.append(json.dumps(data['properties']['periods'][i]["detailedForecast"]))
        table.add_row(templist[0], templist[1], templist[2])
        # print(templist) # temporary to validate this for loop

    print()  
    console.print(table)
    print()


if __name__ == "__main__":
    main()
# print(__name__)